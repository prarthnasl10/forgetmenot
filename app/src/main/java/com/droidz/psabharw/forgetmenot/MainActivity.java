package com.droidz.psabharw.forgetmenot;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class MainActivity extends ActionBarActivity {
    private ArrayList<String> arrayList ;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        arrayList= new ArrayList<String>();
        arrayAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);
        ListView listView = (ListView)findViewById(R.id.listViewToDo);
        listView.setAdapter(arrayAdapter);
        registerForContextMenu(listView);

        try {
                Log.i("On create","Hi, on create has occured.");
            Scanner scanner = new Scanner(openFileInput("todo.txt"));
            while (scanner.hasNextLine()){
                String next = scanner.nextLine();
                arrayAdapter.add(next);
            }
            scanner.close();
        } catch(Exception e){
            Log.i("On create",e.getMessage());
                            }

        }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()!=R.id.listViewToDo){
            return;
        }
        menu.setHeaderTitle("What would you like to do?");
        String[] options = {"Delete","Return"};

        for (String option : options){
            menu.add(option);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int selected = info.position;

        if(item.getTitle()=="Delete") {
            arrayList.remove(selected);
            arrayAdapter.notifyDataSetChanged();
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        try {
            Log.i("On back pressed", "Hi, on back pressed has occured.");

            PrintWriter pw = new PrintWriter(openFileOutput("todo.txt", Context.MODE_PRIVATE));

            for(String todo : arrayList) {
                pw.println(todo);
            }
            pw.close();

        } catch (Exception e) {

            Log.i("On back pressed",e.getMessage());
        }
        finish();
    }

    public void addtolist (View v) {
        final EditText myEditText =(EditText)findViewById(R.id.myEditText);

        String todo = ((EditText)findViewById(R.id.myEditText)).getText().toString().trim();

        if (todo.isEmpty()) {
            return;
        }
        final ImageView imageView=(ImageView)findViewById(R.id.stk);
        final Animation anim = AnimationUtils.loadAnimation(this,R.anim.anim);
        imageView.startAnimation(anim);

        arrayAdapter.add(todo);
        myEditText.setText("");

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
